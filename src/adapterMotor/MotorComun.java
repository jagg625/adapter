package adapterMotor;

public class MotorComun implements MotorNormal{

	@Override
	public void encender() {
		System.out.println("Encendido de un Motor común");
	}

	@Override
	public void acelerar() {
		System.out.println("Aceleración de un Motor común");
	}

	@Override
	public void apagar() {
		System.out.println("Apagado un Motor común");
	}

}
