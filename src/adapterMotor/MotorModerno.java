package adapterMotor;

public interface MotorModerno{
	
	public void conectar();
	
	public void activar();
	
	public void moverMasRapido();
	
	public void detener();
	
	public void desconectar();

}
