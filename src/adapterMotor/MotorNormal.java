package adapterMotor;

public interface MotorNormal {
	
	public void encender();
	
	public void acelerar();
	
	public void apagar();

}
