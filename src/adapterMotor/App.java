package adapterMotor;

public class App {
	
	public void run(){
		MotorNormal motorNormal = new MotorComun();
		
		System.out.println("************ CONFIGURANDO AUTOMOVIL CON MOTOR COMÚN ************");
		
		AutomovilNormal automovilNormal = new AutomovilNormal(motorNormal);
		automovilNormal.encender();
		automovilNormal.acelerar();
		automovilNormal.apagar();
		
		System.out.println("************ CONFIGURANDO AUTOMOVIL CON MOTOR ECONOMICO ************");
		motorNormal = new MotorEconomico();
		automovilNormal.cambiarMotor(motorNormal);
		automovilNormal.encender();
		automovilNormal.acelerar();
		automovilNormal.apagar();
		
		System.out.println("************ CONFIGURANDO AUTOMOVIL CON MOTOR ELECTRICO ************");
		MotorModerno motorModerno = new MotorElectrico();
		motorNormal = new MotorModerno2NormalAdapter(motorModerno);
		automovilNormal.cambiarMotor(motorNormal);
		automovilNormal.encender();
		automovilNormal.acelerar();
		automovilNormal.apagar();
		
	}
}
