package adapterMotor;

public class MotorEconomico implements MotorNormal{

	@Override
	public void encender() {
		System.out.println("Encendido de un Motor económico");
	}

	@Override
	public void acelerar() {
		System.out.println("Aceleración de un Motor económico");
	}

	@Override
	public void apagar() {
		System.out.println("Apagado un Motor económico");
	}

}
